package at.tuwien.prip.common.datastructures;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * AltList.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Sep 10, 2012
 */
public class AltList<T> 
{

	List<List<T>> columns;
	
	/**
	 * 
	 */
	public AltList() {
		columns = new ArrayList<List<T>>();
	}
		
	/**
	 * 
	 * @param index
	 * @param value
	 */
	public void addToColumn(int index, T value)
	{
		List<T> column = null;
		if (columns.size()==0 || index>=columns.size())
		{
			column = new ArrayList<T>();
			columns.add(index, column);
		}
		else 
		{
			column = columns.get(index);
			if (column==null)
			{
				column = new ArrayList<T>();
				columns.add(index, column);
			}
		}
		
		if (!column.contains(value))
		{
			column.add(value);
		}
	}
	
	public List<T> getColumn(int index)
	{
		return columns.get(index);
	}
	
	public int getMaxHeight()
	{
		int max = Integer.MIN_VALUE;
		for (List<T> col : columns)
		{
			if (col.size()>max)
			{
				max = col.size();
			}
		}
		return max;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<List<T>> permute () 
	{
		List<List<T>> result = new ArrayList<List<T>>();
		
		perm(result, 0);
		return result;
	}
	
	private void perm (List<List<T>> list, int col)
	{
		//recurse
		if (columns.size()>col+1)
		{
			perm(list, col+1);
		}
			
		List<List<T>> copy = deepCopy(list);
		
		//for each item
		for (int i=0; i<columns.get(col).size(); i++)
		{
			List<T> tmp = new ArrayList<T>();
			T item = columns.get(col).get(i);
			
			//for each subsequent list
			if (columns.size()<=col+1)
			{
				tmp.add(item);
				list.add(tmp);
			}
			else
			{
				if (i==0)
				{
					for (List<T> l : list)
					{
						l.add(0, item);
					}
				} 
				else
				{
					//duplicate lists
					for (List<T> l : deepCopy(copy))
					{
						l.add(0, item);
						list.add(l);
					}
				}
			}
		}
	}
	
	public static <T> List<List<T>> deepCopy (List<List<T>> original)
	{
		List<List<T>> copy = new ArrayList<List<T>>();
		for (List<T> o : original)
		{
			copy.add(new ArrayList<T>(o));
		}
		return copy;
	}
	
}