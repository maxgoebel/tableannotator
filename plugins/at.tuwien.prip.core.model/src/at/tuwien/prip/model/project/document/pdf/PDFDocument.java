package at.tuwien.prip.model.project.document.pdf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import at.tuwien.prip.model.graph.ISegmentGraph;
import at.tuwien.prip.model.project.document.AbstractDocument;
import at.tuwien.prip.model.project.document.DocumentFormat;

import com.sun.pdfview.PDFFile;

/**
 * PDFDocument.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * May 20, 2012
 */
public class PDFDocument extends AbstractDocument
implements IPdfDocument
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7892878062409340943L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private int numPages;
	
	private List<PdfDocumentPage> pages;
	
	private byte[] binary;
	
	@Transient
	private PDFFile pdfFile;

	private Map<Integer, ISegmentGraph> page2graphMap;
	
	/**
	 * Constructor.
	 */
	public PDFDocument() 
	{
		this.setFormat(DocumentFormat.PDF);
		this.page2graphMap = new HashMap<Integer, ISegmentGraph>();
		this.pages = new ArrayList<PdfDocumentPage>();
	}
	
	/**
	 * 
	 * @param num
	 * @return
	 */
	public PdfDocumentPage getPage(int num) {
		for (PdfDocumentPage page : pages)
		{
			if (page.getPageNum()==num)
				return page;
		}
		return null;
	}

	/**
	 * Add a PDF page.
	 * @param page
	 * @param pageNum
	 */
	public void addPage(PdfDocumentPage page, int pageNum)
	{
		PdfDocumentPage prev = getPage(pageNum);
		if (prev!=null)
		{
			pages.remove(prev);
		}
		pages.add(page);
	}
	
	public int getNumPages() {
		return numPages;
	}

	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}

	public PDFFile getPdfFile() {
		return pdfFile;
	}

	public void setPdfFile(PDFFile pdfFile) {
		this.pdfFile = pdfFile;
	}
	
	public ISegmentGraph getDocumentGraph(int pageNum)
	{
		return page2graphMap.get(pageNum);
	}
	
	public void setDocumentGraph(int pageNum, ISegmentGraph g)
	{
		this.page2graphMap.put(pageNum, g);
	}
	
	public void setBinary(byte[] binary) {
		this.binary = binary;
	}
	
	public byte[] getBinary() {
		return binary;
	}
}
