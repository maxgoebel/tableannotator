/*******************************************************************************
 * Copyright (c) 2013 Max Göbel.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Max Göbel - initial API and implementation
 ******************************************************************************/
package at.tuwien.prip.annotator.editors;

import java.io.File;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.internal.PartSite;
import org.eclipse.ui.internal.PopupMenuExtender;
import org.eclipse.ui.part.EditorPart;

import at.tuwien.prip.annotator.control.DocumentController;
import at.tuwien.prip.annotator.control.DocumentUpdate;
import at.tuwien.prip.annotator.control.DocumentUpdate.UpdateType;
import at.tuwien.prip.annotator.control.DocumentUpdateEvent;
import at.tuwien.prip.annotator.control.IDocumentUpdateListener;
import at.tuwien.prip.annotator.control.SelectionController;
import at.tuwien.prip.annotator.editors.annotator.AnnotatorEditor;
import at.tuwien.prip.annotator.editors.annotator.DocWrapEditor;
import at.tuwien.prip.annotator.editors.annotator.PDFViewerSWT;
import at.tuwien.prip.annotator.editors.base.BaseEditor;
import at.tuwien.prip.annotator.views.BenchmarkEditorInput;
import at.tuwien.prip.annotator.views.anno.AnnotationView;
import at.tuwien.prip.model.graph.ISegmentGraph;
import at.tuwien.prip.model.project.document.DocumentFormat;
import at.tuwien.prip.model.project.document.DocumentModel;
import at.tuwien.prip.model.project.document.IDocument;
import at.tuwien.prip.model.project.document.benchmark.BenchmarkDocument;
import at.tuwien.prip.model.project.document.benchmark.PdfBenchmarkDocument;

/**
 * WrapperEditor.java
 * 
 * 
 * @author mcg <mcgoebel@gmail.com>
 * @date May 25, 2011
 */
public class WrapperEditor extends BaseEditor implements IMenuListener,
		IDocumentUpdateListener {
	public static String ID = "at.tuwien.prip.annotator.wrapperEditor";

	private Map<EditorPart, Integer> editor2PageIndex;
	private BitSet activeEditors = new BitSet();

	/* A Graph editor */
	protected DocWrapEditor graphEditor;
	/* A PDF viewer */
	protected PDFViewerSWT pdfSWTViewer;
	/* A Combi editor */
	protected AnnotatorEditor annoEditor;
	
	/* SINGLETON selection and document controllers */
	public final DocumentController documentControl = new DocumentController();

	public final SelectionController selectionControl = new SelectionController();

	public ISegmentGraph graph;

	/**
	 * Constructor.
	 * 
	 * @param site
	 * @param input
	 */
	public WrapperEditor(IEditorSite site, IEditorInput input) {
		this();

		try {
			init(site, input);
		} catch (PartInitException e) {
			e.printStackTrace();
		}

		if (input instanceof BenchmarkEditorInput) {
			BenchmarkEditorInput eei = (BenchmarkEditorInput) input;
			PdfBenchmarkDocument benchDoc = (PdfBenchmarkDocument) eei
					.getBenchmarkDocument();
//			this.activeDocument = benchDoc;

			DocumentModel model = new DocumentModel();
			model.setDocument(benchDoc);
			model.setNumPages(benchDoc.getNumPages());
			model.setPdfFile(benchDoc.getPdfFile());

			DocumentUpdate update = new DocumentUpdate();
			update.setUpdate(model);
			update.setType(UpdateType.NEW_DOCUMENT);
			documentControl.setDocumentUpdate(update);

			pdfSWTViewer.setMaxPage(benchDoc.getNumPages());
		}
	}

	/**
	 * Constructor.
	 */
	public WrapperEditor() {
		super();

		documentControl.addDocumentUpdateListener(this);

		editor2PageIndex = new HashMap<EditorPart, Integer>();
	}

	@Override
	public String getTitle() {
		return "Wrapper Editor Test";
	}

	/**
	 * Set the status line.
	 * 
	 * @param message
	 */
	public void setStatusLine(String message) {
		// Get the status line and set the text
		IActionBars bars = getEditorSite().getActionBars();
		bars.getStatusLineManager().setMessage(message);
	}

	@Override
	public void showBusy(boolean busy) {
		super.showBusy(busy);
		/* change cursor to/from busy */
		Cursor busyCursor;
		if (busy) {
			busyCursor = new Cursor(getSite().getShell().getDisplay(),
					SWT.CURSOR_WAIT);
		} else {
			busyCursor = new Cursor(getSite().getShell().getDisplay(),
					SWT.CURSOR_ARROW);
		}
		getSite().getShell().setCursor(busyCursor);
	}

	// /**
	// * Change the visible pages of this editor.
	// */
	// private void setForHTML ()
	// {
	// /* remove the PDF page */
	// int bitIndex = editor2PageIndex.get(pdfSWTViewer);
	// if (activeEditors.get(bitIndex)) {
	// if (getPageCount()>2) {
	// removePage(bitIndex);
	// } else {
	// removePage(0);
	// }
	// activeEditors.set(bitIndex, false);
	// }
	//
	// bitIndex = editor2PageIndex.get(webEditor);
	// if (!activeEditors.get(bitIndex))
	// {
	// /* add Web Editor page */
	// try
	// {
	// MozBrowserEditorInput mozInput = null;
	// if (getEditorInput() instanceof IFileEditorInput) {
	// IFileEditorInput fileIn = (IFileEditorInput) getEditorInput();
	// mozInput = new MozBrowserEditorInput(fileIn);
	// }
	//
	// addPage(0, webEditor, mozInput);
	// activeEditors.set(bitIndex);
	//
	// } catch (PartInitException e) {
	// e.printStackTrace();
	// } catch (MalformedURLException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// setPageText(0, "Document View");
	// setActivePage(0);
	// }
	//
	// /**
	// * Change the visible pages of this editor.
	// */
	// private void setForPDF ()
	// {
	// /* remove the WebEditor page */
	// int bitIndex = editor2PageIndex.get(webEditor);
	// if (activeEditors.get(bitIndex)) {
	// if (getPageCount()>2) {
	// removePage(bitIndex);
	// } else {
	// removePage(0);
	// }
	// activeEditors.set(bitIndex, false);
	// }
	//
	// bitIndex = editor2PageIndex.get(pdfSWTViewer);
	// if (!activeEditors.get(bitIndex))
	// {
	// /* add PDF viewer page */
	// try
	// {
	// addPage(0, pdfSWTViewer, getEditorInput());
	// activeEditors.set(bitIndex);
	//
	// } catch (PartInitException e) {
	// e.printStackTrace();
	// }
	// }
	// setPageText(0, "Document View");
	// setActivePage(0);
	// }

	/**
	 * Get the adapter.
	 */
	// @Override
	// public Object getAdapter(Class key)
	// {
	// if (key==IDocumentsViewAdapter.class ||
	// key==IDocumentCollectionContainer.class)
	// {
	// return dcc;
	// }
	// else
	// {
	// Object o = super.getAdapter(key);
	// if (o==null) {
	// //o = fEditor.getAdapter(key);
	// }
	// return o;
	// }
	// }

	@Override
	public void dispose() {
		super.dispose();

		deregisterFromDocumentUpdate(pdfSWTViewer);
		documentControl.removeDocumentUpdateListener(pdfSWTViewer);
		documentControl.removeDocumentUpdateListener(this);
		selectionControl.deregisterComponent(pdfSWTViewer);
	}

	@Override
	protected void createPages()
	{
//		createPage2();
		createPage3();
		createPage1();

		setPartName("Loading...");
	}

	/**
	 * Setup PDF editor.
	 */
	private void createPage1() 
	{
		try
		{
			pdfSWTViewer = new PDFViewerSWT(getEditorSite(), getEditorInput());
			pdfSWTViewer.setWrapperEditor((WrapperEditor) this);
			documentControl.addDocumentUpdateListener(pdfSWTViewer);
			selectionControl.registerComponent(pdfSWTViewer);

			int index = addPage(pdfSWTViewer, getEditorInput());
			setPageText(index, "PDF Viewer");

			editor2PageIndex.put(pdfSWTViewer, index);
			activeEditors.set(index);
		}
		catch (PartInitException e) 
		{
			ErrorDialog.openError(getSite().getShell(),
					"Error creating nested text editor", null, e.getStatus());
		}
	}

	private void createPage2() 
	{
		try 
		{
			/* create a new graph editor */
			annoEditor = new AnnotatorEditor(this, getEditorSite(),
					getEditorInput());

			selectionControl.registerComponent(annoEditor);
			documentControl.addDocumentUpdateListener(annoEditor);

			int index = addPage(annoEditor, getEditorInput());
			setPageText(index, "Combi Editor");

			editor2PageIndex.put(graphEditor, index);
			activeEditors.set(index);
		} 
		catch (PartInitException e) 
		{
			ErrorDialog.openError(getSite().getShell(),
					"Error creating nested text editor", null, e.getStatus());
		}
	}

	/**
	 * Setup Graph Editor.
	 */
	private void createPage3() 
	{
		try 
		{
			/* create a new graph editor */
			graphEditor = new DocWrapEditor(this, getEditorSite(),
					getEditorInput());
			// graphEditor.setWrapperEditor((WrapperEditor) this);

			selectionControl.registerComponent(graphEditor);
			documentControl.addDocumentUpdateListener(graphEditor);

			int index = addPage(graphEditor, getEditorInput());
			setPageText(index, "Layout Graph");

			editor2PageIndex.put(graphEditor, index);
			activeEditors.set(index);
		} 
		catch (PartInitException e) 
		{
			ErrorDialog.openError(getSite().getShell(),
					"Error creating nested text editor", null, e.getStatus());
		}
	}

	/**
	 * 
	 * @param update
	 */
	public void setDocumentUpdated(DocumentUpdate update) {
		documentControl.setDocumentUpdate(update);
	}

	/**
	 * This creates a context menu for the viewer and adds a listener as well
	 * registering the menu for extension.
	 */
	@SuppressWarnings("restriction")
	public Menu createAndRegisterContextMenuFor(StructuredViewer viewer) {
		// EMF drag&drop support
		// super.addEMFDragDropSupportFor(viewer);
		// drag&drop support
		// int dndOperations = DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK;
		// Transfer[] transfers = new Transfer[] { LocalTransfer.getInstance()
		// };
		// viewer.addDragSupport(dndOperations, transfers, new
		// ViewerDragAdapter(viewer));

		MenuManager contextMenu = new MenuManager("#PopUp");
		// contextMenu.add(new Separator("additions"));
		contextMenu.setRemoveAllWhenShown(true);
		contextMenu.addMenuListener(this);
		Menu menu = contextMenu.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		// getSite().registerContextMenu(contextMenu, viewer);

		IWorkbenchPartSite site = getSite();
//		PartSite.registerContextMenu(site.getId(), contextMenu, viewer, false,
//				site.getPart(), new ArrayList<PopupMenuExtender>(1));

		return menu;
	}

	// private long lastStart;
	// public void delayedSelect(final Command cmd, final boolean alsoInOutline)
	// {
	// final long start = System.currentTimeMillis();
	// lastStart = start;
	//
	// final Display d = Display.getCurrent();
	// Thread t = new Thread() {
	// @Override
	// public void run() {
	// try { sleep(400); } catch (InterruptedException e) {
	// ErrorDump.error(this, e); }
	// //check if not canceled
	// if (lastStart<0) return;
	// //check if a new selection was started
	// if (lastStart>start) return;
	//
	// d.syncExec(new Runnable() { public void run() {
	// immediateSelect(cmd, alsoInOutline);
	// }});
	// }};
	// t.start();
	// }
	//
	// public void immediateSelect(Command cmd, boolean alsoInOutline)
	// {
	// ISelection sel = new StructuredSelection(cmd);
	//
	// if (alsoInOutline) {
	// //change outline view selection
	// ISelection cosel = contentOutlinePage.getSelection();
	// if (cosel==null || !sel.equals(cosel)) {
	// contentOutlinePage.setSelection(sel);
	// }
	// }
	//
	// //change properties view
	// ISelection esel = getSelection();
	// if (esel==null || !sel.equals(esel)) {
	// setSelection(sel);
	// }
	// }
	//
	// protected void cancelDelayedSelect() {
	// lastStart=-1;
	// }

	// /**
	// * Closes all project files on project close.
	// */
	// public void resourceChanged(final IResourceChangeEvent event){
	// if(event.getType() == IResourceChangeEvent.PRE_CLOSE){
	// Display.getDefault().asyncExec(new Runnable(){
	// public void run(){
	// IWorkbenchPage[] pages = getSite().getWorkbenchWindow().getPages();
	// for (int i = 0; i<pages.length; i++){
	// if(((FileEditorInput)webEditor.getEditorInput()).getFile().getProject().equals(event.getResource())){
	// IEditorPart editorPart = pages[i].findEditor(webEditor.getEditorInput());
	// pages[i].closeEditor(editorPart,true);
	// }
	// }
	// }
	// });
	// }
	// }

	// @Override
	// public void openURL(URL url) throws PartInitException {
	// webEditor.openURL(url);
	// }

	// public ISelection getDomNodeSelection() {
	// return webEditor.getSelection();
	// }
	//
	// public void clearSelection() {
	// webEditor.setSelection(null);
	// }
	//
	// public WeblearnEditor getWlEditor() {
	// return webEditor;
	// }

	public DocWrapEditor getGraphEditor() {
		return graphEditor;
	}

	// /**
	// * Delegate browser monitor
	// * @param monitor
	// */
	// public void registerBrowserMonitor (BrowserMonitor monitor) {
	// webEditor.registerBrowserMonitor(monitor);
	// }

	public void registerForSelection(ISelectionChangedListener listener) {
		selectionControl.registerComponent(listener);
	}

	public void deregisterFromSelection(ISelectionChangedListener listener) {
		selectionControl.removeSelectionChangedListener(listener);
	}

	public void registerForDocumentUpdate(IDocumentUpdateListener listener) {
		documentControl.addDocumentUpdateListener(listener);
	}

	public void deregisterFromDocumentUpdate(IDocumentUpdateListener listener) {
		documentControl.removeDocumentUpdateListener(listener);
	}

	// public void unregisterBrowserMonitor (BrowserMonitor monitor) {
	// webEditor.unregisterBrowserMonitor(monitor);
	// }
	//
	// public Browser getMozillaBrowser () {
	// if (webEditor.getMozillaBrowser().isDisposed())
	// {
	// return webEditor.getBrowser();
	// }
	// return webEditor.getMozillaBrowser();
	// }
	//
	// public NavigationBar getBrowserNavigationBar() {
	// return webEditor.getNavBar();
	// }
	//
	// public nsIDOMDocument getDocument() {
	// return webEditor.getDocument();
	// }
	//
	// public Action getClickSelectAction() {
	// return webEditor.getClickSelectAction();
	// }
	//
	// public boolean isLoading() {
	// return webEditor.isDocumentLoading();
	// }

	/**
	 * This implements {@link org.eclipse.jface.action.IMenuListener} to help
	 * fill the context menus with contributions from the Edit menu.
	 */
	// OK
	public void menuAboutToShow(IMenuManager menuManager) {
		((IMenuListener) getEditorSite().getActionBarContributor())
				.menuAboutToShow(menuManager);
	}

	public IDocument getActiveDocument() {
		return documentControl.getCurrentDocumentEntry();
	}

	public int getCurrentDocumentPageNum() {
		return documentControl.getCurrentDocumentPage();
	}

	/**
	 * 
	 * @param input
	 */
	public void setInput2(IEditorInput input) {
		if (input instanceof BenchmarkEditorInput) {
			/* create a new document model */
			DocumentModel model = new DocumentModel();

			BenchmarkEditorInput eei = (BenchmarkEditorInput) input;
			BenchmarkDocument benchmark = (BenchmarkDocument) eei
					.getBenchmarkDocument();
			if (!(benchmark instanceof PdfBenchmarkDocument)) {
				model.setDocument((PdfBenchmarkDocument) benchmark);
				model.setFormat(DocumentFormat.PDF);
				model.setUri(benchmark.getFileName());
			} else {
				PdfBenchmarkDocument benchDoc = (PdfBenchmarkDocument) benchmark;
				model.setDocument(benchDoc);
				model.setNumPages(benchDoc.getNumPages());
				model.setPdfFile(benchDoc.getPdfFile());
				model.setFormat(DocumentFormat.PDF);

				pdfSWTViewer.setMaxPage(benchDoc.getNumPages());
			}

			String fileName = benchmark.getFileName();
			fileName = fileName.substring(fileName.lastIndexOf(File.separator)+1, fileName.length());
			setPartName(fileName);

			DocumentUpdate update = new DocumentUpdate();
			update.setUpdate(model);
			update.setProvider(this);
			update.setType(UpdateType.NEW_DOCUMENT);
			setDocumentUpdated(update); // tell self
		}

		// register annotation view as selection listener...
		IWorkbenchPage page = getSite().getPage();
		if (page != null) {
			IViewReference ref = page.findViewReference(AnnotationView.ID);
			if (ref != null) {
				AnnotationView av = (AnnotationView) ref.getPart(true);
				selectionControl.addSelectionChangedListener(av);
			}
		}
	}

	@Override
	public void documentUpdated(DocumentUpdateEvent ev) {
		// DocumentModel model = ev.getDocumentUpdate().getUpdate();
		// DocumentGraph dg = null;
		// ISegmentGraph sg = model.getDocumentGraph();
		// if (sg instanceof ISegHierGraph) {
		// dg = (DocumentGraph) ((ISegHierGraph) sg).getBaseGraph();
		// } else {
		// dg = (DocumentGraph) sg;
		// }

		// this.activeDocument.setPdfFile(model.getPdfFile());
		// this.activeDocument.setFileName(model.getDocument().getUri());
	}

	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		// TODO Auto-generated method stub

	}
	
}// WrapperEditor
