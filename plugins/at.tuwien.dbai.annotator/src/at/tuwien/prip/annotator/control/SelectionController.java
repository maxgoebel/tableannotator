/*******************************************************************************
 * Copyright (c) 2013 Max Göbel.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Max Göbel - initial API and implementation
 ******************************************************************************/
package at.tuwien.prip.annotator.control;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;

import at.tuwien.prip.annotator.editors.annotator.DocWrapEditor;
import at.tuwien.prip.annotator.editors.annotator.PDFViewerSWT;
import at.tuwien.prip.annotator.views.anno.AnnotationView;

/**
 * 
 * SelectionController.java
 * 
 * 
 * @author mcg <mcgoebel@gmail.com>
 * @date Oct 1, 2011
 */
public class SelectionController implements ISelectionProvider,
		ISelectionChangedListener {
	private List<ISelectionChangedListener> listeners;

	private ISelection lastSelection;
	private ISelectionProvider lastProvider;

	public SelectionController() {
		lastProvider = this;
		listeners = new ArrayList<ISelectionChangedListener>();
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		lastProvider = event.getSelectionProvider();
		lastSelection = event.getSelection();

		if (event.getSelectionProvider() instanceof PDFViewerSWT) {
			for (ISelectionChangedListener listener : listeners) {
				if (listener instanceof AnnotationView) {
					listener.selectionChanged(event);
				}
			}
		} else if (event.getSelectionProvider() instanceof DocWrapEditor) {
			for (ISelectionChangedListener listener : listeners) {
				// if (listener instanceof PatternViewClassic)
				// {
				// listener.selectionChanged(event);
				// }
				// else if (listener instanceof PatternViewZest)
				// {
				// listener.selectionChanged(event);
				// }
				// else
				if (listener instanceof AnnotationView) {
					listener.selectionChanged(event);
				}
			}
		}
		// else if (event.getSelectionProvider() instanceof WeblearnEditor)
		// {
		// for (ISelectionChangedListener listener : listeners)
		// {
		// if (listener instanceof PatternViewClassic)
		// {
		// listener.selectionChanged(event);
		// }
		// else if (listener instanceof PatternViewZest)
		// {
		// listener.selectionChanged(event);
		// }
		// }
		// }
		else if (event.getSelectionProvider() instanceof TreeViewer) {
			TreeViewer viewer = (TreeViewer) lastProvider;
			ISelection selection = viewer.getSelection();
			if (selection instanceof TreeSelection) {
				// TreeSelection tsel = (TreeSelection) selection;
				// Object element = tsel.getFirstElement();

				// in case of node selection, notify weblearn editor
				// if (element instanceof NodeSelection) {
				// for (ISelectionChangedListener listener : listeners) {
				// if (listener instanceof WeblearnEditor)
				// {
				// listener.selectionChanged(event);
				// }
				// }
				// }
			}
		}

	}

	/**
	 * 
	 * @param component
	 */
	public void registerComponent(Object component) {

		if (component instanceof ISelectionProvider) {
			((ISelectionProvider) component).addSelectionChangedListener(this);
		}
		if (component instanceof ISelectionChangedListener) {
			addSelectionChangedListener((ISelectionChangedListener) component);
		}
	}

	/**
	 * 
	 * @param component
	 */
	public void deregisterComponent(Object component) {
		if (component instanceof ISelectionProvider) {
			((ISelectionProvider) component)
					.removeSelectionChangedListener(this);
		}
		if (component instanceof ISelectionChangedListener) {
			removeSelectionChangedListener((ISelectionChangedListener) component);
		}
	}

	@Override
	public void addSelectionChangedListener(ISelectionChangedListener listener) {
		if (!this.listeners.contains(listener)) {
			this.listeners.add(listener);
		}
	}

	@Override
	public ISelection getSelection() {
		return lastSelection;
	}

	@Override
	public void removeSelectionChangedListener(
			ISelectionChangedListener listener) {
		this.listeners.remove(listener);

	}

	@Override
	public void setSelection(ISelection selection) {
		if (lastSelection.equals(selection)) {
			return;
		}

		SelectionChangedEvent se = new SelectionChangedEvent(lastProvider,
				selection);
		for (ISelectionChangedListener listener : listeners) {
			listener.selectionChanged(se);
		}

		lastSelection = selection;
	}

}
