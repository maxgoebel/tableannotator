/*******************************************************************************
 * Copyright (c) 2013 Max Göbel.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Lesser Public License v2.1
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * 
 * Contributors:
 *     Max Göbel - initial API and implementation
 ******************************************************************************/
package at.tuwien.prip.annotator.views.bench;

import java.util.Iterator;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import at.tuwien.prip.annotator.Activator;
import at.tuwien.prip.annotator.control.IModelChangedListener;
import at.tuwien.prip.annotator.control.ModelChangedEvent;
import at.tuwien.prip.annotator.editors.WrapperEditor;
import at.tuwien.prip.annotator.editors.annotator.DocWrapEditor;
import at.tuwien.prip.annotator.views.BenchmarkEditorInput;
import at.tuwien.prip.annotator.views.anno.AnnotationView;
import at.tuwien.prip.model.project.document.benchmark.Benchmark;
import at.tuwien.prip.model.project.document.benchmark.BenchmarkDocument;
import at.tuwien.prip.model.project.document.benchmark.BenchmarkModel;

/**
 * BenchmarkNavigatorView.java
 * 
 * 
 * @author mcgoebel@gmail.com
 * @date Feb 18, 2013
 */
public class BenchmarkNavigatorView extends ViewPart implements
ISelectionChangedListener, IModelChangedListener {

	private String iDirname = null;
	private IMemento iMemento = null;
	private TreeViewer iViewer;

	private Action deleteItemAction;

	public static final String ID = "at.tuwien.prip.annotator.views.benchmark";

	public BenchmarkNavigatorView() {
		super();
	}

	public void init(IViewSite site, IMemento memento) throws PartInitException {
		iMemento = memento;
		super.init(site, memento);
	}

	public void createPartControl(Composite parent) {
		Activator.modelControl.addModelChangedListener(this);

		iViewer = new TreeViewer(parent);
		iViewer.setContentProvider(new BenchmarkContentProvider());
		iViewer.setLabelProvider(new BenchmarkLabelProvider());
		restoreState();

		getSite().setSelectionProvider(iViewer);
		makeActions();
		createContextMenu();

		iViewer.addSelectionChangedListener(this);
	}

	private void makeActions()
	{
		deleteItemAction = new Action("Close") {
			public void run() {
				deleteItem();
			}
		};
		Image deleteImage =  PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ETOOL_DELETE);
		deleteItemAction.setImageDescriptor(ImageDescriptor.createFromImage(deleteImage));
		deleteItemAction.setEnabled(false);
	}

	/**
	 *
	 */
	private void createContextMenu()
	{
		// Create menu manager.
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager mgr) {
				fillContextMenu(mgr);
			}
		});

		// Create menu.
		iViewer.getControl().setMenu(
				menuMgr.createContextMenu(iViewer.getControl()));

		// Register menu for extension.
		getSite().registerContextMenu(menuMgr, iViewer);
	}

	/**
	 *
	 * @param mgr
	 */
	private void fillContextMenu(IMenuManager mgr)
	{
		mgr.add(deleteItemAction);
	}

	private void restoreState() {
		if (iMemento == null) {
			if (iDirname == null) {
				iDirname = System.getProperty("user.home");
			}
			return;
		}
		IMemento dirname = iMemento.getChild("directory");
		if (dirname != null) {
			iDirname = dirname.getID();
		}
	}

	public void saveState(IMemento memento) {
		memento.createChild("directory", iDirname);
		super.saveState(memento);
	}

	@Override
	public void setFocus() {

	}

	Object lastSelection = null;
	
	@Override
	public void selectionChanged(SelectionChangedEvent event) 
	{
		if (event.getSelection() == null || !( event.getSelection() instanceof IStructuredSelection))
		{
			return;
		}

		StructuredSelection ss = (StructuredSelection) event.getSelection();
		Object obj = ss.getFirstElement();
		if (obj==null)
			return;

		if (obj==lastSelection)
		{
			return;//nothing to do...
		}

		lastSelection = obj;
		
		if (obj instanceof BenchmarkDocument) 
		{
			BenchmarkDocument benchDoc = (BenchmarkDocument) ss.getFirstElement();

			/* update universal benchmark model */
			BenchmarkModel model = Activator.modelControl.getModel();
			model.setCurrentDocument(benchDoc);
			Activator.modelControl.modelChanged(model);

			/* send selection to PDF editor */
			IWorkbenchPage page = getSite().getPage();
			page = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
			.getActivePage();

			if (page != null && benchDoc != null) 
			{
				try 
				{
					BenchmarkEditorInput input = new BenchmarkEditorInput(benchDoc);
					IEditorPart editor = page.findEditor(input);
					if (editor==null)
					{
						editor = page.openEditor(input, WrapperEditor.ID, true);

					}
					
					WrapperEditor we = (WrapperEditor) editor;
					we.setInput2(input);
					page.activate(editor);
					
					//set selection to annotation view
					IViewPart view = page.findView(AnnotationView.ID);
					if (view!=null)
					{
						AnnotationView annoView = (AnnotationView) view;
						annoView.setInput(benchDoc);
					}

					DocWrapEditor active = we.getGraphEditor();
					active.refresh();
					active.canvas.calibrateGraphPosition();
					
				} catch (PartInitException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 *
	 */
	public void deleteItem()
	{
		IStructuredSelection sel =
			(IStructuredSelection)iViewer.getSelection();

		TreeSelection tsel  = null;
		Object parent = null;
		if (sel instanceof TreeSelection)
		{
			tsel = (TreeSelection) sel;
			TreePath paths = tsel.getPathsFor(tsel.getFirstElement())[0];
			parent = paths.getParentPath().getLastSegment();
		}

		Iterator<?> iter = sel.iterator();
		while (iter.hasNext())
		{
			Object obj = iter.next();


			BenchmarkModel model = Activator.modelControl.getModel();
			Iterator<BenchmarkDocument> bdIter = model.getBenchmarks().get(0).getDocuments().iterator();
			while (bdIter.hasNext())
			{
				BenchmarkDocument bd = bdIter.next();
				if (obj.equals(bd)) {
					bdIter.remove();
					break;
				}
			}

			/* send selection to PDF editor */
			IWorkbenchPage page = getSite().getPage();
			page = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
			.getActivePage();

			if (page != null) 
			{
				BenchmarkEditorInput input = new BenchmarkEditorInput((BenchmarkDocument) obj);
				IEditorPart editor = page.findEditor(input);
				if (editor!=null)
				{
					page.closeEditor(editor, false);
				}

				//set selection to annotation view
				IViewPart view = page.findView(AnnotationView.ID);
				if (view!=null)
				{
					AnnotationView annoView = (AnnotationView) view;
					annoView.setInput(null);
				}
			}

			/* update universal benchmark model */
			model.setCurrentDocument(null);
			Activator.modelControl.modelChanged(model);
		}
		iViewer.refresh();
	}

	@Override
	public void modelChanged(ModelChangedEvent event) {
		// simply refresh
		getSite().getShell().getDisplay().asyncExec(new Runnable() {
			public void run() {
				BenchmarkModel model = Activator.modelControl.getModel();
				if (model!=null && model.getBenchmarks().size()>0)
				{
					if (iViewer!=null)
					{
						Benchmark bench = model.getBenchmarks().get(0);
						if (bench!=null)
						{
							iViewer.setInput(bench.getDocuments());

							if (bench.getDocuments().size()==1)
							{
								BenchmarkDocument selection = bench.getDocuments().get(0);
								//auto-select new document
								if (selection!=null)
								{
									TreePath path = new TreePath(new Object[]{selection});
									TreeSelection treeSel = new TreeSelection(path);
									iViewer.setSelection(treeSel);
								}
							}
						}
					}
					IStructuredSelection sel = (IStructuredSelection)iViewer.getSelection();
					deleteItemAction.setEnabled(sel.size() > 0);
				}
			}
		});
	}
}
