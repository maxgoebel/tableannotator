package at.tuwien.prip.annotator.utils.benchmark;

import java.awt.Rectangle;
import java.io.File;
import java.io.FilenameFilter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import at.tuwien.prip.annotator.utils.PdfDocumentUtils;
import at.tuwien.prip.common.utils.XMLDocument;
import at.tuwien.prip.model.project.annotation.AnnotationPage;
import at.tuwien.prip.model.project.annotation.TableAnnotation;
import at.tuwien.prip.model.project.document.benchmark.BenchmarkDocument;
import at.tuwien.prip.model.project.selection.RegionSelection;
import at.tuwien.prip.model.project.selection.TableCell;
import at.tuwien.prip.model.project.selection.TableSelection;
import at.tuwien.prip.model.utils.DOMHelper;

public class OmniPageTableTransform {

	public static void main(String[] args)
	{
		String outDir = "/home/max/Desktop/omnipage/transformed";
		String rootDirName = "/home/max/Desktop/omnipage/icdar2013-omnipage18prof-results";
		File rootDir = new File(rootDirName);
		if (rootDir.exists() && rootDir.isDirectory())
		{
			String[] fileNames = rootDir.list(new FilenameFilter() {
				
				@Override
				public boolean accept(File arg0, String arg1) {
					if (arg1.endsWith(".xml"))
						return true;
					return false;
				}
			});
			
			for (String fileName : fileNames)
			{
				String path = rootDirName + File.separator + fileName;
				BenchmarkDocument document = transform(path);
				DiademBenchmarkEngine.writeTableBenchmark(document, outDir + File.separator + fileName);
			}
		}
	}
	
	/**
	 * 
	 * @param input
	 * @return
	 */
	public static BenchmarkDocument transform (String input)
	{
		BenchmarkDocument result = new BenchmarkDocument();
		result.setFileName(input.substring(input.lastIndexOf(File.separator)+1));
		result.setUri(input);
		result.setName(input.substring(input.lastIndexOf(File.separator)+1));
		result.setTimeStamp(new Date(System.currentTimeMillis()));
		TableAnnotation anno = new TableAnnotation(input);
		result.getAnnotations().add(anno);
		
		double transformRatio = 0;
					 		
		XMLDocument xml = new XMLDocument();
		URI uri;
		try 
		{
			uri = new URI(input);
			Document doc = xml.parse2document(uri);
			
			//find all pages
			int pageNum = 1;
			int tableId = 0;
			NodeList pageNL = doc.getElementsByTagName("page");
			for (int pg=0; pg<pageNL.getLength(); pg++)
			{
				Element pageEl = (Element) pageNL.item(pg);
				List<Element> theoreticals = DOMHelper.Tree.Descendant.getFirstNamedDescendantsAndSelfElements(
						pageEl,
						"theoreticalPage");	

				//process PDF for size transform
				String pdfFileName = input.substring(0, input.lastIndexOf("."));
				pdfFileName += ".pdf";				
				Rectangle pdfBounds = PdfDocumentUtils.getPdfPageBounds(pdfFileName, pageNum);
				
				if (theoreticals.size()>0)
				{
					Element theoretical = theoreticals.get(0);
					double pageWidth = Double.parseDouble(theoretical.getAttribute("width"));
					double pageHeight = Double.parseDouble(theoretical.getAttribute("height"));
					double transformRatio2 = pdfBounds.getHeight()/pageHeight;
					transformRatio = pdfBounds.getWidth()/pageWidth;		
					
					if (transformRatio!=transformRatio2)
					{
						System.err.println("Document proportions mismatch: "+Math.abs(transformRatio-transformRatio2));
					}
				}
				
				//find all tables
				NodeList tableNL = pageEl.getElementsByTagName("table");
				for (int i=0; i<tableNL.getLength(); i++)
				{
					RegionSelection region = new RegionSelection();
					Rectangle regionBounds = null;
					
					TableSelection table = new TableSelection();
					table.setId(tableId);
					
					AnnotationPage page = new AnnotationPage(pageNum);
					
					Element tableElem = (Element) tableNL.item(i);
					int x1 = (int) (Integer.parseInt(tableElem.getAttribute("l")) * transformRatio);
					int x2 = (int) (Integer.parseInt(tableElem.getAttribute("r")) * transformRatio);
					int y1 = (int) (Integer.parseInt(tableElem.getAttribute("b")) * transformRatio);
					int y2 = (int) (Integer.parseInt(tableElem.getAttribute("t")) *transformRatio);
					regionBounds = new Rectangle(x1,pdfBounds.height - y1,Math.abs(x2-x1),Math.abs(y2-y1));
					region.setBounds(regionBounds);
					
					//find all cells
					int cellId = 0;
					List<Element> cells = DOMHelper.Tree.Children.getChildElements(tableElem, "cell");
					for (Element cellElem : cells)
					{
						TableCell cell = new TableCell(cellId);

						//col and row coefficients
						int startCol = Integer.parseInt(cellElem.getAttribute("gridColFrom"));
						int endCol = Integer.parseInt(cellElem.getAttribute("gridColTill"));
						int startRow = Integer.parseInt(cellElem.getAttribute("gridRowFrom"));
						int endRow = Integer.parseInt(cellElem.getAttribute("gridRowTill"));
						cell.setStartCol(startCol);
						cell.setEndCol(endCol);
						cell.setStartRow(startRow);
						cell.setEndRow(endRow);
						
						//bounding box
						Rectangle bounds = new Rectangle();
						List<Element> params = DOMHelper.Tree.Children.getChildElements(cellElem, "para");
						if (params.size()>0)
						{
							Element para = params.get(0);
							x1 = (int) (Integer.parseInt(para.getAttribute("l")) * transformRatio);
							x2 = (int) (Integer.parseInt(para.getAttribute("r")) * transformRatio);
							y1 = (int) (Integer.parseInt(para.getAttribute("b")) * transformRatio);
							y2 = (int) (Integer.parseInt(para.getAttribute("t")) * transformRatio);
							bounds = new Rectangle(x1,pdfBounds.height - y1,Math.abs(x2-x1),Math.abs(y2-y1));
							
							//content, all lines
							StringBuffer sb = new StringBuffer();
							List<Element> lines = DOMHelper.Tree.Children.getChildElements(para, "ln");
							for (Element line : lines)
							{
								NodeList nl = line.getChildNodes();
								for (int k=0; k<nl.getLength(); k++)
								{
									Node item = nl.item(k);
									String nn = item.getNodeName();
									if ("wd".equalsIgnoreCase(nn))
									{
										sb.append(item.getTextContent());
									}
									else if ("space".equalsIgnoreCase(nn))
									{
										sb.append(" ");
									}
									else
									{
										String text = item.getTextContent();
										if ("\n".equals(text))
										{
										}
										else
										sb.append(text);
									}
								}
							}
							
							String content = sb.toString();
							cell.setContent(content);
						}
						else
						{
							List<Element> pictures = DOMHelper.Tree.Children.getChildElements(cellElem, "picture");
							if (pictures.size()>0)
							{
								Element pic = pictures.get(0);
								x1 = (int) (Integer.parseInt(pic.getAttribute("l")) * transformRatio);
								x2 = (int) (Integer.parseInt(pic.getAttribute("r")) * transformRatio);
								y1 = (int) (Integer.parseInt(pic.getAttribute("b")) * transformRatio);
								y2 = (int) (Integer.parseInt(pic.getAttribute("t")) * transformRatio);
								bounds = new Rectangle(x1,pdfBounds.height - y1,Math.abs(x2-x1),Math.abs(y2-y1));
							}
						}
						
						cell.setBounds(bounds);
						region.getCellContainer().add(cell);
						cellId++;
					}
					
					//add the region to the page
					page.getItems().add(region);
					
					//add the page to the table
					table.getPages().add(page);
					tableId++;
					
					//add the table to the annotation
					anno.getItems().add(table);
				}
				pageNum++;
			}
		} 
		catch (URISyntaxException e) 
		{
			e.printStackTrace();
		}
		return result;
	}
}
