package at.tuwien.prip.annotator.utils;

public enum GraphDetail {

	CHAR, WORD, LINE, BLOCK, FRAGMENT
}
