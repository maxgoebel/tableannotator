package at.tuwien.prip.annotator.utils;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author max
 *
 * @param <T>
 */
public class Cluster<T>
	{
		List<T> nodes;

		Rectangle bounds;
		
		public Cluster(T node)
		{
			this.nodes = new ArrayList<T>();
			nodes.add(node);
			
//			this.bounds = node.getBoundingBox().getBounds();
		}

		public Cluster(Cluster<T> a, Cluster <T>b)
		{
			//merge two clusters
			this.nodes = new ArrayList<T>();
			if (a!=null)
			{
				this.nodes.addAll(a.nodes);
			}

			if (b!=null)
			{
				for (T bn : b.nodes)
				{
					if (!this.nodes.contains(bn))
					{
						this.nodes.add(bn);
					}
				}			
			}
			
			if (a!=null && b!=null)
			{
				this.bounds = a.bounds.union(b.bounds);
			}
		}

		@Override
		public boolean equals(Object obj) 
		{
			if (obj instanceof Cluster)
			{
				Cluster<T> other = (Cluster<T>) obj;

				if (this.nodes.size()==other.nodes.size())
				{
					List<T> disjoint = new ArrayList<T>(this.nodes);
					disjoint.removeAll(other.nodes);
					return disjoint.isEmpty();
				}
			}
			return false;
		}

	}
