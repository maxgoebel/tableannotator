package at.tuwien.prip.annotator.utils;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import at.ac.tuwien.dbai.pdfwrap.analysis.PageProcessor;
import at.ac.tuwien.dbai.pdfwrap.exceptions.DocumentProcessingException;
import at.ac.tuwien.dbai.pdfwrap.utils.ProcessFile;
import at.tuwien.prip.model.document.segments.CharSegment;
import at.tuwien.prip.model.document.segments.GenericSegment;
import at.tuwien.prip.model.document.segments.ImageSegment;
import at.tuwien.prip.model.document.segments.OpTuple;
import at.tuwien.prip.model.document.segments.Page;
import at.tuwien.prip.model.document.segments.TextBlock;
import at.tuwien.prip.model.document.segments.TextLine;
import at.tuwien.prip.model.document.segments.fragments.LineFragment;
import at.tuwien.prip.model.document.segments.fragments.TextFragment;
import at.tuwien.prip.model.graph.AdjacencyGraph;
import at.tuwien.prip.model.graph.DocumentGraph;
import at.tuwien.prip.model.utils.DocGraphUtils;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

public class PdfDocumentUtils
{
	/**
	 * Process a PDF document (one page only).
	 * 
	 * @param inFile
	 * @param page, the page number to process
	 */
	public static Rectangle getPdfPageBounds(String inFile, int page)
	{
		File file = null;
		try 
		{
			URL url = null;
			if (inFile.startsWith("file:")) {
				url = new URL(inFile);
			} else {
				url = new File(inFile).toURI().toURL();
			}

			if (url == null) {
				return null;
			}

			file = new File(url.toURI());
			if (file.exists()) {
				File tmpDir = new File(System.getenv("HOME") + File.separator
						+ ".docwrap");
				if (!tmpDir.exists()) {
					tmpDir.mkdirs();
				}
				File dest = new File(tmpDir, "thumb.png");
				if (!dest.exists()) {
					dest.createNewFile();
				}
			}
		}
		catch(MalformedURLException e)
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		catch (URISyntaxException e) 
		{
			e.printStackTrace();
		}
		
		Rectangle result = null;
		RandomAccessFile raf = null;
		try
		{
			/* load PDF file */
			raf = new RandomAccessFile(file, "r");									// "r");
			FileChannel fc = raf.getChannel();
			ByteBuffer buf = fc.map(FileChannel.MapMode.READ_ONLY, 0,
					fc.size());
			PDFFile pdfFile = new PDFFile(buf);

			PDFPage pg = pdfFile.getPage(page);
			result = pg.getBBox().getBounds();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if (raf!=null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		return result;
	}

	/**
	 * 
	 * @param inFile
	 * @param page
	 * @return
	 * @throws PdfDocumentProcessingException
	 * @throws DocumentProcessingException
	 */
	public static DocumentGraph generateDocumentGraphNew(String inFile,	int page) 
			throws PdfDocumentProcessingException 
			{
		DocumentGraph result = new DocumentGraph();

		try 
		{
			File inputDocFile;
			if (inFile.startsWith("file://")) {
				inputDocFile = new File(inFile.substring(5));
			} else if (inFile.startsWith("file:/")) {
				inputDocFile = new File(inFile.substring(5));
			} else {
				inputDocFile = new File(inFile);
			}

			byte[] inputDoc = ProcessFile.getBytesFromFile(inputDocFile);

			List<AdjacencyGraph<GenericSegment>> adjGraphList = new ArrayList<AdjacencyGraph<GenericSegment>>();

			//read detail level from preferences
			int detailLevel = PageProcessor.PP_BLOCK;
			boolean wordLevel = false;



			// process the PDF
			List<Page> pages = null;
			try 
			{
				pages = ProcessFile.processPDF(inputDoc,
						detailLevel,// PageProcessor.PP_LINE,//LINE, //block
						// contains all
						null, 
						false, //compute edges
						false, false, page, page, "", "", 0,
						adjGraphList, false);
			} 
			catch (DocumentProcessingException e)
			{
				throw new PdfDocumentProcessingException(e);
			}

			if (pages == null || adjGraphList == null
					|| adjGraphList.size() == 0) {
				return null;
			}

			AdjacencyGraph<GenericSegment> g = adjGraphList.get(0);

			/***********************************************************************
			 * split all segments according to their segmentation level...
			 */
			List<GenericSegment> blocks = new ArrayList<GenericSegment>();
			List<GenericSegment> lines = new ArrayList<GenericSegment>();
			List<GenericSegment> words = new ArrayList<GenericSegment>();
			List<GenericSegment> chars = new ArrayList<GenericSegment>();
			List<GenericSegment> imgs = new ArrayList<GenericSegment>();
			List<GenericSegment> instr = new ArrayList<GenericSegment>();


			List<GenericSegment> segments = g.getVertList();
			List<OpTuple> pdfOps4 = new ArrayList<OpTuple>();
			for (GenericSegment segment : segments) 
			{
				if (segment instanceof TextBlock)
				{
					List<OpTuple> pdfOps3 = new ArrayList<OpTuple>();
					TextBlock block = (TextBlock) segment;
					for (TextLine line : block.getItems()) 
					{
						List<OpTuple> pdfOps2 = new ArrayList<OpTuple>();
						//						words.addAll(convertTextLineToWordFragments(line));

						for (LineFragment lineFrag : line.getItems()) 
						{
							List<OpTuple> pdfOps1 = new ArrayList<OpTuple>();
							List<TextFragment> textFrags = lineFrag.getItems();

							for (TextFragment tFrag : textFrags)
							{
								// System.out.println(tFrag.toExtendedString());
								chars.addAll(tFrag.getItems());

								/* remember PDF operators */
								List<OpTuple> pdfOps = new ArrayList<OpTuple>();

								for (CharSegment cs : tFrag.getItems()) 
								{
									instr.add(cs);
									pdfOps.add(cs.getSourceOp());
								}
								tFrag.setOperators(pdfOps);
								pdfOps1.addAll(pdfOps);
							}
							lines.add(lineFrag);
							lineFrag.setOperators(pdfOps1);
							pdfOps2.addAll(pdfOps1);
						}
						line.setOperators(pdfOps2);
						pdfOps3.addAll(pdfOps2);
					}
					pdfOps4.addAll(pdfOps3);
					block.setOperators(pdfOps3);
					blocks.add(block);
				} 
				else if (segment instanceof ImageSegment) 
				{
					ImageSegment img = (ImageSegment) segment;
					imgs.add(img);
				}
				segment.setOperators(pdfOps4);
			}

			// generate the TEXTBLOCK level graph
			AdjacencyGraph<GenericSegment> g1 = new AdjacencyGraph<GenericSegment>();
			if (wordLevel)
			{
				g1.addList(words);
			} 
			else if (detailLevel == PageProcessor.PP_LINE) 
			{
				g1.addList(lines);
			}
			else if (detailLevel == PageProcessor.PP_CHAR) 
			{
				g1.addList(chars);
			}
			else if (detailLevel == PageProcessor.PP_INSTRUCTION) 
			{
				g1.addList(instr);
			}

			// add images...
			g1.addList(imgs);

			// compute edges
			//			g1.generateEdgesSingle();
			result = new DocumentGraph(g1);

			// result = new DocumentGraph(g);
			DocGraphUtils.flipReverseDocGraph(result);
			int pgCnt = pages.size();
			result.setNumPages(pgCnt); // remember page count

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
			}
}