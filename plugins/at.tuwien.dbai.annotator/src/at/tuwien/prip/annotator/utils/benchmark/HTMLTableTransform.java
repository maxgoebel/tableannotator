//package at.tuwien.prip.annotator.utils.benchmark;
//
//import java.io.File;
//import java.io.FilenameFilter;
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.util.Date;
//
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;
//
//import at.tuwien.prip.model.project.annotation.AnnotationPage;
//import at.tuwien.prip.model.project.annotation.TableAnnotation;
//import at.tuwien.prip.model.project.document.benchmark.BenchmarkDocument;
//import at.tuwien.prip.model.project.selection.RegionSelection;
//import at.tuwien.prip.model.project.selection.TableCell;
//import at.tuwien.prip.model.project.selection.TableSelection;
//
///**
// *
// * @author max
// *
// */
//public class HTMLTableTransform 
//{
//
//	/**
//	 * Main method.
//	 * @param args
//	 */
//	public static void main(String[] args)
//	{
//		if (args.length!=2)
//		{
//			usage();
//			return;
//		}
//		
//		String inDir = args[0];
//		String outDir = args[1];
//		
//		File rootDir = new File(inDir);
//		if (rootDir.exists())
//		{
//			String[] fileNames = null;
//			if (rootDir.isDirectory())
//			{
//				fileNames = rootDir.list(new FilenameFilter() 
//				{
//					@Override
//					public boolean accept(File arg0, String arg1) {
//						if (arg1.endsWith(".html"))
//							return true;
//						return false;
//					}
//				});
//			}
//			if (fileNames!=null)
//			{
//				for (String fileName : fileNames)
//				{
//					String path = inDir + File.separator + fileName;
//					BenchmarkDocument document = transform(path);
//					DiademBenchmarkEngine.writeTableBenchmark(document, outDir + File.separator + fileName);
//				}
//			}
//			else
//			{
//				System.err.println("Input file does not resolve.");
//				return;
//			}
//		}
//		else
//		{
//			System.err.println("Input file does not resolve.");
//			usage();
//			return;
//		}
//	}
//	
//	public static void usage()
//	{
//		System.out.println("Convert HTML files to table XML.");
//		System.out.println("The program takes as input argument the paht of an HTML file or a directory containing a set of HTML files, " +
//				"and as second argument an output directory where the table XML transform will be saved to.");
//	}
//
//	/**
//	 * 
//	 * @param input
//	 * @return
//	 */
//	public static BenchmarkDocument transform (String input)
//	{
//		BenchmarkDocument result = new BenchmarkDocument();
//		result.setFileName(input.substring(input.lastIndexOf(File.separator)+1));
//		result.setUri(input);
//		result.setName(input.substring(input.lastIndexOf(File.separator)+1));
//		result.setTimeStamp(new Date(System.currentTimeMillis()));
//		TableAnnotation anno = new TableAnnotation(input);
//		result.getAnnotations().add(anno);
//
//
//		try 
//		{
//
//			File inFile = new File(input);
//			Document doc = Jsoup.parse(inFile, "UTF-8");
//
//			//find all tables
//			int tableId = 0;
//			Elements tableNL = doc.getElementsByTag("table");
//			for (int pg=0; pg<tableNL.size(); pg++)
//			{
//				Element tableElem = (Element) tableNL.get(pg);
//
//				TableSelection table = new TableSelection();
//				table.setId(tableId);
//
//				AnnotationPage page = new AnnotationPage();
//				RegionSelection region = new RegionSelection();
//
//				//find all table rows
//				int startRow = 0;
//				int cellId = 0;
//
//				Elements rows = tableElem.getElementsByTag("tr");
//				for (Element rowElem : rows)
//				{
//					Elements cells = rowElem.getElementsByTag("td");
//
//					int startCol = 0;
//					for (Element cellElem : cells)
//					{
//						TableCell cell = new TableCell(cellId);
//
//						//col and row coefficients
//						int rowSpan = 1;
//						int colSpan = 1;
//
//						if (cellElem.attr("rowspan").length()>0)
//							Integer.parseInt(cellElem.attr("rowspan"));
//						if (cellElem.attr("colspan").length()>0)
//							Integer.parseInt(cellElem.attr("colspan"));
//
//						int endCol = startCol + colSpan -1;
//						int endRow = startRow + rowSpan -1;
//
//						cell.setStartCol(startCol);
//						cell.setEndCol(endCol);
//						cell.setStartRow(startRow);
//						cell.setEndRow(endRow);
//
//						cell.setContent(cellElem.text());
//						region.getCellContainer().add(cell);
//						cellId++;
//
//						startCol += colSpan;
//					}
//					startRow++; //new row
//				}
//				//add the region to the page
//				page.getItems().add(region);
//
//				//add the page to the table
//				table.getPages().add(page);
//				tableId++;
//
//				//add the table to the annotation
//				anno.getItems().add(table);
//			}
//		} 
//		//		catch (URISyntaxException e) 
//		//		{
//		//			e.printStackTrace();
//		//		}
//		catch (MalformedURLException e) 
//		{
//			e.printStackTrace();
//		} 
//		catch (IOException e) 
//		{
//			e.printStackTrace();
//		}
//		return result;
//	}
//}
