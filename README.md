# README #

### Annotation Workflow ###

TableAnnotator is a GUI tool to annotate tables in PDF documents for benchmarking. In the file view (right pane), the tool shows the individual PDF commands as blue rectangles. At the bottom left, click the 'create new table button', to create a new table annotation for the currently loaded file. In the sane view, select the table you wish to edit (e.g. the newly created one), and select the objects from PDF file in the right pane that belong to this table by drawing a selection box over their respective blue outlines on the right. The selected commands are now added to the table model you have selected in the bottom left view. When done annotating all tables, click the 'save' button in the bottom left to persist you annotations to XML.

### Running the Software ###

* From the Downloads page of this project, download the zipped binary matching your environment
* Extract on local disk
* Find the pdfAnnotator launcher and double click to start the software
* Go to File - Open PDF to load a document into the tool

### For Development ###

* TableAnnotator is a SWT project (eclipse)
* Download the latest RCP/RAP version of eclipse from eclipse.org/downloads
* Go to workspace, import projects, GIT - projects from Git - Clone URI (or clone repos locally first and import from local Git repo)
* Enter git address 'git@bitbucket.org:maxgoebel/tableannotator.git' into prompt -> Import
* Install the GEF package (for draw2d) using HELP -> INSTALL NEW SOFTWARE in eclipse
* Check if everything compiles
* Open the PdfAnnotator.product file in the at.tuwien.dbai.annotator project
* Under 'dependencies' tab, click 'select required', make sure no errors
* Under 'overview' tab click 'Run as Eclipse Application'